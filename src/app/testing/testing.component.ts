import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

import { FetchService } from '../fetch.service';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css'],
})
export class TestingComponent implements OnInit {
  title = 'RxJS Practice';
  public fetchData: any = [];

  public totalItems: number = this.fetchData.length;
  itemsPerPage = 1;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageIndex: number = 1;
  // public page = 1;
  pageEvent: PageEvent | undefined;

  @Output()
  onChanged = new EventEmitter<string>();

  @Input()
  placeholder = '';

  @Input()
  value = '';

  constructor(private service: FetchService) {}

  ngOnInit(): void {
    this.fetch();
  }

  fetch(): void {
    this.service.getData().subscribe((response) => {
      let start = (this.pageIndex - 1) * this.itemsPerPage;
      let end = start + this.itemsPerPage;
      this.fetchData = response.slice(start, end);
      this.totalItems = response.length;
      console.log(`start ${start} end ${end}`);
      console.log(this.fetchData);
    });
    /// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
  }

  onChange(e: string) {
    console.log(e);
    this.onChanged.emit(e);
    for (let i = 0; i < this.fetchData.length; i++) {
      const data = this.fetchData[i].name;
      if ((e = data)) {
      }
    }
  }

  onPaginate(pageEvent: PageEvent) {
    console.log(pageEvent);
    this.itemsPerPage = pageEvent.pageSize;
    this.pageIndex = pageEvent.pageIndex + 1;
    this.fetch();
  }
}
